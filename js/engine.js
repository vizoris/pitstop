
$(function() {




// MMENU
var menu = new MmenuLight(
  document.querySelector( '#mobile-menu' ),
  'all'
);

var navigator = menu.navigation({
  // selectedClass: 'Selected',
  // slidingSubmenus: true,
  // theme: 'dark',
   title: 'Меню'
});

var drawer = menu.offcanvas({
  // position: 'left'
});

//  Open the menu.
document.querySelector( 'a[href="#mobile-menu"]' )
  .addEventListener( 'click', evnt => {
    evnt.preventDefault();
    drawer.open();
});



// Выпадающее меню
if ($(window).width() >= 992) {
   $('.dropdown, .dropdown li').hover(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   });

}else {
  $('.dropdown').click(function() {
    $(this).children('ul').stop(true,true).fadeToggle();
   })
}



 // Стилизация селектов
$('select').styler();


// Маска телефона
$(".phone-mask").mask("+7 (999) 99-99-999");




//  Слайдер партнерев
$('.partners-slider').slick({
  dots: false,
  arrows: false,
  speed: 300,
  slidesToShow: 6,
  slidesToScroll: 1,
  responsive: [
   {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
      }
    }
  ]
});




//Табы
$('.tabs-nav a').click(function() {
    var _id = $(this).attr('href');
    var _targetElement = $('.tabs-wrap').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});



//Табы карты
$('.tabs-nav__item').click(function() {
    var _id = $(this).attr('data-map');
    var _targetElement = $('.tabs-wrap').find('#' + _id);
    $('.tabs-nav__item').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;

});




var productSlider = $('.product-slider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.product-slider__nav'
});


$('.product-slider__nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.product-slider',
  dots: false,
  arrows: true,
  focusOnSelect: true,
  responsive: [
  {
    breakpoint: 768,
    settings: {
      slidesToShow: 2,
    }
  },
  ]
});


// FansyBox
 $('.fancybox').fancybox({});



// Показать описание продукта
$('.product-description__body .btn').click(function() {
  $(this).parent().toggleClass('active')
  $(this).fadeToggle();
})


$('.faq-header').click(function(){
  if(!($(this).next().hasClass('active'))){
    $('.faq-body').slideUp().removeClass('active');
    $(this).next().slideDown().addClass('active');
  }else{
    $('.faq-body').slideUp().removeClass('active');
  };

  if(!($(this).parent('.faq-item').hasClass('active'))){
    $('.faq-item').removeClass("active");
    $(this).parent('.faq-item').addClass("active");
  }else{
    $(this).parent('.faq-item').removeClass("active");
  };
});






// сайдбар на мобильном
$('.sidebar-mobile__btn').click(function() {
  $(this).toggleClass('active');
  $('.sidebar').toggleClass('active')
})



// Каталог карусель
var catalogSlider =  $('.catalog-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: false,
    pauseOnHover: false,
    dots: false,
    responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
     {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 479,
      settings: {
        slidesToShow: 1,
      }
    }
    ]
});
$('.catalog-slider__prev').click(function(){
  $(catalogSlider).slick("slickNext");
 });
 $('.catalog-slider__next').click(function(){
  $(catalogSlider).slick("slickPrev");
 });



// Прикрепляем файлы

$('.attach').each(function() { // на случай, если таких групп файлов будет больше одной
  var attach = $(this),
    fieldClass = 'attach__item', // класс поля
    attachedClass = 'attach__item--attached', // класс поля с файлом
    fields = attach.find('.' + fieldClass).length, // начальное кол-во полей
    fieldsAttached = 0; // начальное кол-во полей с файлами

  var newItem = '<div class="attach__item"><label><div class="attach__up">Добавить файл</div><input class="attach__input" type="file" name="files[]" /></label><div class="attach__delete"><i class="sprite sprite-cross"></i></div><div class="attach__name"></div><div class="attach__edit">Изменить</div></div>'; // разметка нового поля

  // При изменении инпута
  attach.on('change', '.attach__input', function(e) {
    var item = $(this).closest('.' + fieldClass),
      fileName = '';
    if (e.target.value) { // если value инпута не пустое
      fileName = e.target.value.split('\\').pop(); // оставляем только имя файла и записываем в переменную
    }
    if (fileName) { // если имя файла не пустое
      item.find('.attach__name').text(fileName); // подставляем в поле имя файла
      if (!item.hasClass(attachedClass)) { // если в поле до этого не было файла
        item.addClass(attachedClass); // отмечаем поле классом
        fieldsAttached++;
      }
      if (fields < 10 && fields == fieldsAttached) { // если полей меньше 10 и кол-во полей равно
        item.after($(newItem)); // добавляем новое поле
        fields++;
      }
    } else { // если имя файла пустое
      if (fields == fieldsAttached + 1) {
        item.remove(); // удаляем поле
        fields--;
      } else {
        item.replaceWith($(newItem)); // заменяем поле на "чистое"
      }
      fieldsAttached--;

      if (fields == 1) { // если поле осталось одно
        attach.find('.attach__up').text('Загрузить файл'); // меняем текст
      }
    }
  });

  // При нажатии на "Изменить"
  attach.on('click', '.attach__edit', function() {
    $(this).closest('.attach__item').find('.attach__input').trigger('click'); // имитируем клик на инпут
  });

  // При нажатии на "Удалить"
  attach.on('click', '.attach__delete', function() {
    var item = $(this).closest('.' + fieldClass);
    if (fields > fieldsAttached) { // если полей больше, чем загруженных файлов
      item.remove(); // удаляем поле
      fields--;
    } else { // если равно
      item.after($(newItem)); // добавляем новое поле
      item.remove(); // удаляем старое
    }
    fieldsAttached--;
    if (fields == 1) { // если поле осталось одно
      attach.find('.attach__up').text('Загрузить файл'); // меняем текст
    }
  });
});



// Begin of slider-range
$( "#slider-range" ).slider({
  range: true,
  min: 8000,
  max: 90000,
  values: [ 20000, 56000 ],
  slide: function( event, ui ) {
    $( "#amount" ).val( ui.values[ 0 ] );
    $( "#amount-1" ).val( ui.values[ 1 ] );
  }
});
$( "#amount" ).val( $( "#slider-range" ).slider( "values", 0 ) );
$( "#amount-1" ).val( $( "#slider-range" ).slider( "values", 1 ) );

      // Изменение местоположения ползунка при вводиде данных в первый элемент input
      $("input#amount").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();
        if(parseInt(value1) > parseInt(value2)){
          value1 = value2;
          $("input#amount").val(value1);
        }
        $("#slider-range").slider("values",0,value1); 
      });
      
      // Изменение местоположения ползунка при вводиде данных в второй элемент input  
      $("input#amount-1").change(function(){
        var value1=$("input#amount").val();
        var value2=$("input#amount-1").val();

        if(parseInt(value1) > parseInt(value2)){
          value2 = value1;
          $("input#amount-1").val(value2);
        }
        $("#slider-range").slider("values",1,value2);
      });
// END of slider-range





})